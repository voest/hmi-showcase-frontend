import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {MessageEvent} from './event-hub.service';


@Injectable()
export class MockEventHubService {

  private readonly MOCK_EVENTS: MessageEvent[] = [
    {
      'source': 'Asset1',
      'timestamp': 1234567,
      'position': {'latitude': 53.233433, 'longitude': -14.23234, 'altitude': 0.0, 'accuracy': 0.0},
      'zone': 'Zone B',
      'alarm': false
    },
    {
      'source': 'Asset2',
      'timestamp': 1234567,
      'position': {'latitude': 53.233433, 'longitude': -14.23234, 'altitude': 0.0, 'accuracy': 0.0},
      'zone': 'Zone D',
      'alarm': false
    },
    {
      'source': 'Asset1',
      'timestamp': 1234567,
      'position': {'latitude': 53.233433, 'longitude': -14.23234, 'altitude': 0.0, 'accuracy': 0.0},
      'zone': 'Zone A',
      'alarm': false
    },
    {
      'source': 'Asset2',
      'timestamp': 1234567,
      'position': {'latitude': 53.233433, 'longitude': -14.23234, 'altitude': 0.0, 'accuracy': 0.0},
      'zone': 'Zone C',
      'alarm': false
    },
  ];

  private events$: Observable<MessageEvent>;

  constructor() {
    this.events$ = Observable.interval(5000).map((value) => {
      return this.MOCK_EVENTS[value % this.MOCK_EVENTS.length];
    });
  }

  public listenEvents(): Observable<MessageEvent> {
    return this.events$;
  }

}
