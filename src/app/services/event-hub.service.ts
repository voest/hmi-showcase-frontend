import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {AppConstants} from '../app-constants';
import {environment} from '../../environments/environment';


export interface Position {
  latitude: number;
  longitude: number;
  altitude: number;
  accuracy: number;
}

export interface MessageEvent {
  source: string;
  timestamp: number;
  position: Position;
  zone: string;
  alarm: boolean;
}

@Injectable()
export class EventHubService {

  private events$: Observable<MessageEvent>;
  private url = environment.URL_EVENTS_WEBSOCKET;


  constructor() {
    const eventSubject = new Subject<MessageEvent>();

    Observable.webSocket<MessageEvent>(this.url)
      .retry()
      .subscribe((event: MessageEvent) => {
        eventSubject.next(event);
      });

    this.events$ = eventSubject.asObservable();
  }

  public listenEvents(): Observable<MessageEvent> {
    return this.events$;
  }

  /**
   * Throotle all events filtered by asset by transation duration,
   * so that assets don´t swing
   * @returns {Observable<MessageEvent>}
   */
  public listenEventsThrottled(): Observable<MessageEvent> {
    const assetObservables = [];
    for (const key of Object.keys(AppConstants.ASSET_IDS)) {
      const assetName = AppConstants.ASSET_IDS[key];
      const asset$ = this.events$.filter(event => event.source === assetName)
        // .distinctUntilChanged((event1, event2) => event1.zone === event2.zone && event1.alarm === event2.alarm)
        // .throttleTime(AppConstants.TRANSITION_DURATION);
      assetObservables.push(asset$);
    }
    return Observable.merge(...assetObservables);
  }

}
