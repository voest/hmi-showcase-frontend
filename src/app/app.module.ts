import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {FabricComponent} from './fabric/fabric.component';
import {CommonModule} from '@angular/common';
import {AppRoutingModule} from './app-routing.module';

import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/dom/webSocket';
import 'rxjs/add/observable/merge';
// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/concat';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/throttleTime';
import 'rxjs/add/operator/filter';
import {EventHubService} from './services/event-hub.service';
import {StockBuildingComponent} from './stock-building/stock-building.component';
import {AutonomLiftComponent} from './autonom/autonom.component';


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    FabricComponent,
    StockBuildingComponent,
    AutonomLiftComponent,
  ],
  providers: [
    EventHubService,
    // { provide: EventHubService, useClass: MockEventHubService},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
