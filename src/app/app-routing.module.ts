import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FabricComponent} from './fabric/fabric.component';
import {StockBuildingComponent} from './stock-building/stock-building.component';
import {AutonomLiftComponent} from './autonom/autonom.component';

const routes: Routes = [
  { path: '', redirectTo: '/stock', pathMatch: 'full' },
  { path: 'fabric', component: FabricComponent},
  { path: 'stock', component: StockBuildingComponent},
  { path: 'autonom', component: AutonomLiftComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
