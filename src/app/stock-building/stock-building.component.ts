import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventHubService} from '../services/event-hub.service';
import {Subscription} from 'rxjs/Subscription';
import {AppConstants} from '../app-constants';

interface AssetConfig {
  id: string;
  availableZones: string[];
  activeZone: string;
  alarm?: boolean;
  displayWarning?: boolean;
}

@Component({
  selector: 'app-stock-building',
  templateUrl: './stock-building.component.html',
  styleUrls: ['./stock-building.component.scss']
})
export class StockBuildingComponent implements OnInit, OnDestroy {

  public ZONE_IDENTIFIER = AppConstants.ZONE_IDS;

  public assets: AssetConfig[] = [
    {
      id: AppConstants.ASSET_IDS.ASSET1,
      availableZones: [AppConstants.ZONE_IDS.ZONE1, AppConstants.ZONE_IDS.ZONE2, AppConstants.ZONE_IDS.ZONE3, AppConstants.ZONE_IDS.ZONE4],
      activeZone: AppConstants.ZONE_IDS.ZONE3
    },
    {
      id: AppConstants.ASSET_IDS.ASSET2,
      availableZones: [AppConstants.ZONE_IDS.ZONE1, AppConstants.ZONE_IDS.ZONE2, AppConstants.ZONE_IDS.ZONE3, AppConstants.ZONE_IDS.ZONE4],
      activeZone: AppConstants.ZONE_IDS.ZONE4
    }
  ];

  private subscription: Subscription;

  constructor(private eventHubService: EventHubService) {
  }

  ngOnInit() {

    this.subscription = this.eventHubService.listenEventsThrottled().subscribe((event) => {
      console.log('Throttled event received:', event);

      const asset = this.assets.find((assetTmp) => assetTmp.id === event.source);
      if (!asset) {
        console.error(`Unknown asset with id ${event.source} was scanned`);
        return;
      }

      asset.alarm = event.alarm;
      if (!event.alarm) {
        const assetMoved = this.moveAssetToZone(asset, event.zone);
        if (assetMoved) {
          asset.displayWarning = false;
        }
      } else {
        asset.displayWarning = true;
      }
    });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private moveAssetToZone(asset: AssetConfig, zone: string): boolean {
    const oldZone = asset.activeZone;
    if (asset.availableZones.indexOf(zone) !== -1) {
      if (asset.activeZone !== zone) {
        asset.activeZone = zone;
      }
    } else {
      console.warn(`Asset ${asset} was moved to zone "${zone}", that is not applicable.`);
    }
    return oldZone !== asset.activeZone;
  }

}
