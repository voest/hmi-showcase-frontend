export class AppConstants {

  /**
   * Standard duration of transition events
   */
  public static readonly TRANSITION_DURATION = 1500;

  public static readonly ZONE_IDS = {
    ZONE1: 'Zone 1',
    ZONE2: 'Zone 2',
    ZONE3: 'Zone 3',
    ZONE4: 'Zone 4',
    ZONE5: 'Zone 5',
    ZONE6: 'Zone 6',
  };

  public static readonly ASSET_IDS = {
    ASSET1: 'Asset 1',
    ASSET2: 'Asset 2',
    ASSET3: 'Asset 3',
  };
}
