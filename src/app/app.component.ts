import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {EventHubService} from './services/event-hub.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['../assets/css/style.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  constructor(private eventHubService: EventHubService) {
  }

  ngOnInit() {
    this.eventHubService.listenEvents().subscribe((event) => {
        console.debug('Event received', event);
      },
      (error) => console.warn('Websocket error', error),
      () => console.error('MessageEvent connection was closed'));
  }
}
