import {Component, OnDestroy, OnInit} from '@angular/core';
import {EventHubService} from '../services/event-hub.service';
import {Subscription} from 'rxjs/Subscription';
import {AppConstants} from '../app-constants';

@Component({
  selector: 'app-fabric',
  templateUrl: './fabric.component.html',
  styleUrls: ['./fabric.component.scss']
})
export class FabricComponent implements OnInit, OnDestroy {

  static readonly FADEOUT_ANIMATION_DELAY: number = 700;

  public assetZone: string = AppConstants.ZONE_IDS.ZONE3;
  public leavingAssetZone: string;
  public ZONE_IDENTIFIER = AppConstants.ZONE_IDS;

  private subscription: Subscription;

  constructor(private eventHubService: EventHubService) {
  }

  ngOnInit() {

    this.subscription = this.eventHubService.listenEvents().subscribe((event) => {
      console.log('Event received:', event);
      if (!event.alarm) {
        this.moveAssetToZone(event.zone);
      } else {
        // TODO implement alarm
        console.log('TODO implement alarm');
      }
    });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private moveAssetToZone(zone: string) {
    this.leavingAssetZone = this.assetZone;
    setTimeout(() => {
      this.assetZone = zone;
    }, FabricComponent.FADEOUT_ANIMATION_DELAY - 50);
  }

}
