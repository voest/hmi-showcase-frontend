export const environment = {
  production: true,
  URL_EVENTS_WEBSOCKET: 'wss://hmi-event-hub.azurewebsites.net/ws-events',
};
